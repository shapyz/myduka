import Vue from 'vue'
import App from './App.vue'
import Router from './route/routes.js'
import axios from 'axios'
import Auth from './packages/Auth/auth.js'

Vue.use(axios)
Vue.use(Auth)
new Vue({
  el: '#app',
  render: h => h(App),
  data: {
    text: "hello world",
  },
  router: Router
})
