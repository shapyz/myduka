export default function(Vue){
    Vue.auth = {
          //set token

          //retrieve token
          setToken: (token, expiration) => {
              localStorage.setItem('token', token)
              localStorage.setItem('expiration', expiration)
          },
          //destroy token
          //is authenticated

          isAuthenticated: () =>{
              if(this.getToken())
                  return true
              else
                  return false
              
          }
    }
    Object.defineProperties(Vue.prototype,{
        $auth: {
            get: () =>{
                return Vue.auth
            }
        }
    })
}