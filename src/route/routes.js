import Vue from 'vue'
import Route from 'vue-router'
import Login from '../components/authentication/login.vue'
import Register from '../components/authentication/register.vue'
import Home from '../components/home.vue'

Vue.use(Route)

const router = new Route({
    routes: [
        {
            path: '/home',
            component: Home
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/register',
            component: Register
        }
    ]
})

export default router
